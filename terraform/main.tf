//variable "ssh_key_name" {}

terraform {
  required_version = ">= 0.12.0"
}

# Configure aws provider
provider "aws" {
  version = ">= 2.11"
  region  = var.region
}

##################################################################
# Data sources to get VPC, subnet, security group and AMI details
##################################################################
data "aws_vpc" "default" {
  default = true
}

data "aws_subnet_ids" "all" {
  vpc_id = data.aws_vpc.default.id
}

data "aws_subnet" "all-individual" {
  count = length(data.aws_subnet_ids.all.ids)
  id    = element(tolist(data.aws_subnet_ids.all.ids), count.index)
}

module "security_group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "3.0.1"

  name        = "example"
  description = "Security group for example usage with EC2 instance"
  vpc_id      = data.aws_vpc.default.id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  ingress_rules       = ["all-tcp", "all-icmp"]
  egress_rules        = ["all-all"]
}

module "ssh_security_group" {
  source  = "terraform-aws-modules/security-group/aws//modules/ssh"
  version = "3.0.1"

  name        = "ssh-group"
  description = "Security group for sshing into EC2 instance"
  vpc_id      = data.aws_vpc.default.id

  ingress_cidr_blocks = ["0.0.0.0/0"]
}

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "5.0.0"

  cluster_name              = "k8-cluster-${var.cluster_id}"
  cluster_security_group_id = "module.security_group.this_security_group_id"
  cluster_version           = "1.13"

  subnets = data.aws_subnet_ids.all.ids

  vpc_id = data.aws_vpc.default.id

  worker_groups = [
    {
      instance_type        = var.node_type
      asg_desired_capacity = var.node_min_size
      asg_max_size         = var.node_max_size
      asg_min_size         = var.node_min_size
      ebs_optimized        = var.ebs_optimized
      key_name             = var.key_pair_name
    },
  ]

  worker_security_group_id = module.security_group.this_security_group_id
  worker_additional_security_group_ids = [ module.ssh_security_group.this_security_group_id ]
}


###############################
# To configure efs
###############################

module "nfs_security_group" {
  source  = "terraform-aws-modules/security-group/aws//modules/nfs"
  version = "3.0.1"

  name        = "nfs-group"
  description = "Security group for allowing nfs on EFS"
  vpc_id      = data.aws_vpc.default.id

  ingress_cidr_blocks = data.aws_subnet.all-individual.*.cidr_block
}

resource "aws_efs_file_system" "trovekube-data" {
  creation_token = "trovekube-data-1"

  tags = {
    Name = "Trovekube Data"
  }
}

output "efs_trovekube_data_id" {
  value = aws_efs_file_system.trovekube-data.id
}

output "efs_region" {
  value = var.region
}

resource "aws_efs_mount_target" "alpha" {
  count = length(data.aws_subnet_ids.all.ids)
  file_system_id = aws_efs_file_system.trovekube-data.id
  subnet_id      = element(tolist(data.aws_subnet_ids.all.ids), count.index)
  security_groups = [ module.nfs_security_group.this_security_group_id ]
}
